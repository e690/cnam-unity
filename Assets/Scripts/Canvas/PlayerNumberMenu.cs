using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNumberMenu : MonoBehaviour
{
    public void SetPlayerGame(int playerNumber)
    {
        RaceManager.Instance.PlayerNumber = playerNumber;
    }
}
