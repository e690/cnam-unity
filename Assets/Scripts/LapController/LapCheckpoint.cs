using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LapCheckpoint : MonoBehaviour
{
    [HideInInspector]
    public int Index;

    private void Awake()
    {
        Index = Int32.Parse(gameObject.name);
    }
    private void OnTriggerEnter(Collider other)
    {
        
        LapController lapController = GetComponentInParent<LapController>();
        if (lapController)
        {
            foreach (GameObject car in lapController.Cars)
            {
                if(other.GetComponentInParent<CarController>() == car.transform.GetChild(0).GetComponent<CarController>())
                {
                    lapController.UpdateCheckpointIndex(this, car);
                }
            }

        }
        
    }
}
