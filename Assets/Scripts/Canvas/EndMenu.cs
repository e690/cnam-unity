using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndMenu : MonoBehaviour
{
    public void BackAction()
    {
        RaceManager.Instance = null;
        SceneManager.LoadScene(0);
    }
    private void Awake()
    {
        transform.Find("WinnerName").GetComponent<TMP_Text>().text = $"{RaceManager.Instance.Winner} !";
        transform.Find("TimeSection/Value").GetComponent<TMP_Text>().text = RaceManager.Instance.WinnerTime.ToString(@"mm\:ss\:fff");
    }
}
