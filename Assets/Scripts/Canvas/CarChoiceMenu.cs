using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class CarChoiceMenu : MonoBehaviour
{
    public int playerIndex = 0;

    public void ChoiceCar()
    {
        RaceManager.Instance.Players[RaceManager.Instance.PlayersName[playerIndex]] = EventSystem.current.currentSelectedGameObject.GetComponent<CarChoice>().datas;
        playerIndex++;
        if (playerIndex < RaceManager.Instance.PlayerNumber)
        {
            Awake();
        }
        else if (playerIndex == RaceManager.Instance.PlayerNumber)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    public void BackAction()
    {
        if (playerIndex == 0)
        {
            FindObjectOfType<PlayerNameMenu>(true).gameObject.SetActive(true);
            gameObject.SetActive(false);
            return;
        }
        playerIndex--;
        RaceManager.Instance.Players[RaceManager.Instance.PlayersName[playerIndex]] = null;
        Awake();

    }

    private void Awake()
    {
        EventSystem.current.SetSelectedGameObject(null);
        transform.Find("FullTitle/PlayerName").GetComponent<TMP_Text>().text = RaceManager.Instance.PlayersName[playerIndex];
    }
}
