using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="CarData", menuName ="ScriptableObjects/Car")]
public class CarData : ScriptableObject
{
    public string carName;
    // Mesh without Colliders
    public GameObject carPrefab;
    // Mesh with Colliders and possible script (Prefab)
    public GameObject carPrefabColliders;
    public float motorForce;
    public float brakeForce;
    public float maxSteeringAngle;
}
