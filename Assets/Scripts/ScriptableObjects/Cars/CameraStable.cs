using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStable : MonoBehaviour
{
    public GameObject Follower;
    private float carY;

    private void Start()
    {
        Follower = transform.parent.gameObject;
    }
    private void FixedUpdate()
    {
        SetAxis();
        LockCube();
    }

    private void SetAxis()
    {
        carY = Follower.transform.eulerAngles.y;
    }

    private void LockCube()
    {
        transform.eulerAngles = new Vector3(0, carY, 0);
    }
}
