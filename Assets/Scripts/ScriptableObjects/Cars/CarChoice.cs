using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CarChoice : MonoBehaviour
{
    public CarData datas;

    private void Awake()
    {
        transform.Find("Name").GetComponent<TMP_Text>().text = datas.carName;
        transform.Find("Motor Force/Value").GetComponent<TMP_Text>().text = datas.motorForce.ToString();
        transform.Find("Brake Force/Value").GetComponent<TMP_Text>().text = datas.brakeForce.ToString();
        transform.Find("Angle/Value").GetComponent<TMP_Text>().text = $"{datas.maxSteeringAngle}�";
    }
}
