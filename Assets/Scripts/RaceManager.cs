using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaceManager : MonoBehaviour
{
    public static RaceManager Instance;
    public int PlayerNumber;
    public List<string> PlayersName = new List<string>();
    public Dictionary<string, CarData> Players = new Dictionary<string, CarData>();
    public string Winner;
    public TimeSpan WinnerTime;
    private void Awake()
    {
        if (Instance)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
}
