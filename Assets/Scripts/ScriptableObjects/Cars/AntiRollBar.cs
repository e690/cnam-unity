using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiRollBar : MonoBehaviour
{
    private const float ANTI_ROLL_VALUE = 5000f;

    public WheelCollider WheelLeft;
    public WheelCollider WheelRight;

    private void Awake()
    {
        WheelLeft = (WheelCollider)gameObject.GetComponentsInChildren<Collider>()[1];
        WheelRight = (WheelCollider)gameObject.GetComponentsInChildren<Collider>()[2];
    }

    private void FixedUpdate()
    {
        float travelLeft = 1.0f;
        float travelRight = 1.0f;

        bool groundedLeft = WheelLeft.GetGroundHit(out WheelHit hit);
        if(groundedLeft)
        {
            travelLeft = (-WheelLeft.transform.InverseTransformPoint(hit.point).y - WheelLeft.radius) / WheelLeft.suspensionDistance;
        }

        bool groundedRight = WheelRight.GetGroundHit(out hit);
        if (groundedRight)
        {
            travelRight = (-WheelRight.transform.InverseTransformPoint(hit.point).y - WheelRight.radius) / WheelRight.suspensionDistance;
        }

        float antiRollForce = (travelLeft - travelRight) * ANTI_ROLL_VALUE;

        if (groundedLeft)
        {
            GetComponent<Rigidbody>().AddForceAtPosition(WheelLeft.transform.up * -antiRollForce, WheelLeft.transform.position);
        }

        if (groundedRight)
        {
            GetComponent<Rigidbody>().AddForceAtPosition(WheelRight.transform.up * antiRollForce, WheelRight.transform.position);
        }


    }
}
