using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    private float horizontalInput;
    private float verticalInput;
    private bool isBraking;
    private bool wantRestart;

    private float currentSteerAngle;
    
    private bool restartKeyPressed;

    private const string HORIZONTAL = "Horizontal";
    private const string VERTICAL = "Vertical";
    private const string BRAKE = "Brake";
    private const string RESTART = "Restart";

    public float currentBrakeForce;

    [SerializeField] public float motorForce;
    [SerializeField] public float brakeForce;
    [SerializeField] public float maxSteeringAngle;
    [SerializeField] public int playerIndex;

    public bool canMove = true;

    [HideInInspector]
    private WheelCollider frontLeftWheelCollider;
    [HideInInspector]
    private WheelCollider frontRightWheelCollider;
    [HideInInspector]
    private WheelCollider rearLeftWheelCollider;
    [HideInInspector]
    private WheelCollider rearRightWheelCollider;

    [HideInInspector]
    private Transform frontLeftWheelTransform;
    [HideInInspector]
    private Transform frontRightWheelTransform;
    [HideInInspector]
    private Transform rearLeftWheelTransform;
    [HideInInspector]
    private Transform rearRightWheelTransform;

    private void Awake()
    {
        SetColliders();
        SetTransforms();
    }
    private void FixedUpdate()
    {
        if(canMove)
        {
            GetInput();
            HandleMotor();
            HandleSteering();
            UpdateWheels();
        }
    }

    private void OnGUI()
    {
        if (wantRestart)
        {
            if (!restartKeyPressed)
            {
                // @TODO Restart method
                restartKeyPressed = true;
                LapController lapController = FindObjectOfType<LapController>();
                RestartToLastCheckpoint(lapController);
            }
        }
        else
        {
            restartKeyPressed = false;
        }
    }

    private void RestartToLastCheckpoint(LapController lapController)
    {
        if (lapController)
        {
            lapController.SetGameObjectPositionToLastCheckpoint(gameObject);
        }
    }

    private void SetColliders()
    {
        frontLeftWheelCollider = (WheelCollider)gameObject.GetComponentsInChildren<Collider>()[1];
        frontRightWheelCollider = (WheelCollider)gameObject.GetComponentsInChildren<Collider>()[2];
        rearLeftWheelCollider = (WheelCollider)gameObject.GetComponentsInChildren<Collider>()[3];
        rearRightWheelCollider = (WheelCollider)gameObject.GetComponentsInChildren<Collider>()[4];
    }

    private void SetTransforms()
    {
        frontLeftWheelTransform = gameObject.GetComponentsInChildren<Transform>()[4];
        frontRightWheelTransform = gameObject.GetComponentsInChildren<Transform>()[5];
        rearLeftWheelTransform = gameObject.GetComponentsInChildren<Transform>()[6];
        rearRightWheelTransform = gameObject.GetComponentsInChildren<Transform>()[7];
    }

    protected void GetInput()
    {
        horizontalInput = Input.GetAxis($"{HORIZONTAL}_{playerIndex}");
        verticalInput = Input.GetAxis($"{VERTICAL}_{playerIndex}");
        isBraking = Input.GetAxis($"{BRAKE}_{playerIndex}") != 0;
        wantRestart = Input.GetAxisRaw($"{RESTART}_{playerIndex}") != 0;
    }

    private void HandleMotor()
    {
        frontLeftWheelCollider.motorTorque = verticalInput * motorForce;
        frontRightWheelCollider.motorTorque = verticalInput * motorForce;
        currentBrakeForce = isBraking ? brakeForce : 0f;
        ApplyBreakingForce();
        
    }

    public void ApplyBreakingForce()
    {
        frontLeftWheelCollider.brakeTorque = currentBrakeForce;
        frontRightWheelCollider.brakeTorque = currentBrakeForce;
        rearLeftWheelCollider.brakeTorque = currentBrakeForce;
        rearRightWheelCollider.brakeTorque = currentBrakeForce;
    }

    private void HandleSteering()
    {
        currentSteerAngle = maxSteeringAngle * horizontalInput;
        frontLeftWheelCollider.steerAngle = currentSteerAngle;
        frontRightWheelCollider.steerAngle = currentSteerAngle;
    }

    private void UpdateWheels()
    {
        UpdateSingleWheel(frontLeftWheelCollider, frontLeftWheelTransform);
        UpdateSingleWheel(frontRightWheelCollider, frontRightWheelTransform);
        UpdateSingleWheel(rearLeftWheelCollider, rearLeftWheelTransform);
        UpdateSingleWheel(rearRightWheelCollider, rearRightWheelTransform);
    }

    private void UpdateSingleWheel(WheelCollider wheelCollider, Transform wheelTransform)
    {
        wheelCollider.GetWorldPose(out Vector3 position, out Quaternion rotation);
        wheelTransform.SetPositionAndRotation(position, rotation);
    }
}
