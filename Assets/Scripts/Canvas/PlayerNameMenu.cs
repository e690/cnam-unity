using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerNameMenu : MonoBehaviour
{
    public void CreatePlayer()
    {
        RaceManager.Instance.PlayersName.Add(GetComponentInChildren<TMP_InputField>().text);
        RaceManager.Instance.Players.Add(RaceManager.Instance.PlayersName[RaceManager.Instance.Players.Count], null);
        if (RaceManager.Instance.PlayersName.Count < RaceManager.Instance.PlayerNumber)
        {
            GetComponentInChildren<TMP_InputField>().text = "";
            Awake();
        }
        else if (RaceManager.Instance.PlayersName.Count == RaceManager.Instance.PlayerNumber)
        {
            FindObjectOfType<CarChoiceMenu>(true).gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    public void BackAction()
    {
        if (RaceManager.Instance.PlayersName.Count == 0)
        {
            FindObjectOfType<PlayerNumberMenu>(true).gameObject.SetActive(true);
            gameObject.SetActive(false);
            return;
        }
        GetComponentInChildren<TMP_InputField>().text = RaceManager.Instance.PlayersName[RaceManager.Instance.PlayersName.Count - 1];
        RaceManager.Instance.Players.Remove(RaceManager.Instance.PlayersName[RaceManager.Instance.PlayersName.Count - 1]);
        RaceManager.Instance.PlayersName.Remove(RaceManager.Instance.PlayersName[RaceManager.Instance.PlayersName.Count - 1]);
        Awake();

    }

    private void Awake()
    {
        transform.Find("Title").GetComponent<TMP_Text>().text = $"Player {RaceManager.Instance.PlayersName.Count + 1} Name:";
    }
}
