using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

public class LapController : MonoBehaviour
{
    private readonly Dictionary<GameObject, int> LastCheckpointIndex = new Dictionary<GameObject, int>();
    [HideInInspector]
    public int CheckpointAmount = 0;
    public int maxLapNumber = 3;

    [HideInInspector]
    public readonly List<GameObject> Cars = new List<GameObject>();
    [HideInInspector]
    public readonly Dictionary<GameObject, int> CurrentLapNumber = new Dictionary<GameObject, int>();
    [HideInInspector]
    public readonly Dictionary<GameObject, int> PositionCars = new Dictionary<GameObject, int>();
    [HideInInspector]
    public readonly Dictionary<GameObject, Stopwatch> StopwatchCars = new Dictionary<GameObject, Stopwatch>();
    private readonly Dictionary<Tuple<GameObject, int, int>, TimeSpan> TimeCheckpoint = new Dictionary<Tuple<GameObject, int, int>, TimeSpan>();
    private void Start()
    {
        foreach (Transform child in transform)
        {
            if (child.TryGetComponent(out LapCheckpoint checkpoint) && checkpoint.Index > CheckpointAmount)
            {
                CheckpointAmount = checkpoint.Index;
            }
        }

        foreach (CarControl carControl in FindObjectsOfType<CarControl>())
        {
            Cars.Add(carControl.gameObject);
        }

        foreach (GameObject car in Cars)
        {
            CurrentLapNumber.Add(car, 1);
            LastCheckpointIndex.Add(car, 0);
            StopwatchCars.Add(car, new Stopwatch());
            PositionCars.Add(car, 1);
        }
    }
    private void FixedUpdate()
    {
        UpdateCarsPosition();
    }

    private void UpdateCarsPosition()
    {
        List<GameObject> carOrder = new List<GameObject>();
        foreach(GameObject car in Cars)
        {
            if(carOrder.Count == 0)
            {
                carOrder.Add(car);
                continue;
            }
            GameObject nextCheckpoint = GameObject.Find(LastCheckpointIndex[car].ToString());
            Tuple<int, int, float> tupleCar = new Tuple<int, int, float>(CurrentLapNumber[car], LastCheckpointIndex[car], Vector3.Distance(car.transform.position, nextCheckpoint.transform.position));
            int indexCar = 0;
            int positionedCarsCount = carOrder.Count;
            while(indexCar < positionedCarsCount)
            {
                GameObject carPositioned = carOrder[indexCar];
                if (ReferenceEquals(carPositioned, car))
                {
                    indexCar++;
                    continue;
                }
                Tuple<int, int, float> tupleCarPositioned = new Tuple<int, int, float>(CurrentLapNumber[carPositioned], LastCheckpointIndex[carPositioned], Vector3.Distance(carPositioned.transform.position, nextCheckpoint.transform.position));
                if (tupleCar.Item1 < tupleCarPositioned.Item1)
                {
                    indexCar++;
                    continue;
                }
                else if (tupleCar.Item1 > tupleCarPositioned.Item1)
                {
                    carOrder.Insert(carOrder.IndexOf(carPositioned), car);
                }
                else
                {
                    if (tupleCar.Item2 < tupleCarPositioned.Item2)
                    {
                        if (carOrder.IndexOf(carPositioned) + 1 == carOrder.Count)
                        {
                            carOrder.Insert(carOrder.Count - 1, car);
                        }
                        indexCar++;
                        continue;
                    }
                    else if (tupleCar.Item2 > tupleCarPositioned.Item2)
                    {
                        carOrder.Insert(carOrder.IndexOf(carPositioned), car);
                    }
                    else
                    {
                        if(tupleCar.Item3 < tupleCarPositioned.Item3)
                        {
                            if (carOrder.IndexOf(carPositioned) + 1 == carOrder.Count)
                            {
                                carOrder.Insert(carOrder.Count - 1, car);
                            }
                            indexCar++;
                            continue;
                        } else
                        {
                            carOrder.Insert(carOrder.IndexOf(carPositioned), car);
                        }
                    }
                }
                indexCar++;
            }
        }
        foreach (GameObject car in carOrder)
        {
            PositionCars[car] = carOrder.IndexOf(car) + 1;
        }
    }

    public void UpdateCheckpointIndex(LapCheckpoint lapCheckpoint, GameObject car)
    {
        if(TimeCheckpoint.ContainsKey(new Tuple<GameObject, int, int>(car, CurrentLapNumber[car], lapCheckpoint.Index))) {
            return;
        }
        StopwatchCars[car].Stop();
        if (lapCheckpoint.Index == CheckpointAmount)
        {
            if(TimeCheckpoint.ContainsKey(new Tuple<GameObject, int, int>(car, CurrentLapNumber[car], CheckpointAmount - 1)))
            {
                CurrentLapNumber[car]++;
            }
            if (CurrentLapNumber[car] > maxLapNumber)
            {
                SetWinner(car);
                return;
            }
            else
            {
                StopwatchCars[car].Restart();
            }
        }
        else 
        {
            if (lapCheckpoint.Index == 1)
            {
                TimeCheckpoint.Add(
                    new Tuple<GameObject, int, int>(car, CurrentLapNumber[car], lapCheckpoint.Index),
                    StopwatchCars[car].Elapsed
                );
            } else if (TimeCheckpoint.ContainsKey(new Tuple<GameObject, int, int>(car, CurrentLapNumber[car], lapCheckpoint.Index - 1))
                && !TimeCheckpoint.ContainsKey(new Tuple<GameObject, int, int>(car, CurrentLapNumber[car], lapCheckpoint.Index)))
            {
                TimeCheckpoint.Add(
                    new Tuple<GameObject, int, int>(car, CurrentLapNumber[car], lapCheckpoint.Index),
                    StopwatchCars[car].Elapsed - TimeCheckpoint[new Tuple<GameObject, int, int>(car, CurrentLapNumber[car], lapCheckpoint.Index - 1)]
                );
            }
            LastCheckpointIndex[car] = lapCheckpoint.Index;
            StopwatchCars[car].Start();
        }
    }

    public void SetGameObjectPositionToLastCheckpoint(GameObject carObject)
    {
        foreach (GameObject carControl in Cars)
        {
            if(carControl.GetComponentInChildren<CarController>().gameObject == carObject)
            {
                foreach (LapCheckpoint checkpoint in GetComponentsInChildren<LapCheckpoint>())
                {
                    if (checkpoint.Index == LastCheckpointIndex[carControl])
                    {
                        carObject.transform.SetPositionAndRotation(checkpoint.transform.position, carControl.GetComponent<CarControl>().rotationOnStart * checkpoint.transform.rotation);
                        carObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
                        carObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
                        carObject.GetComponent<Rigidbody>().Sleep();
                    }
                }
            }
        }
    }

    public void SetWinner(GameObject car)
    {
        RaceManager.Instance.Winner = car.GetComponent<CarControl>().playerName;
        RaceManager.Instance.WinnerTime = StopwatchCars[car].Elapsed;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
