using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarControl : MonoBehaviour
{
    public string playerName;
    public int playerIndex;
    // Car File containing stats
    public CarData carData;
    public Quaternion rotationOnStart;
    public static bool splitScreen = false;

    private void Awake()
    {
        RaceManager raceManager = FindObjectOfType<RaceManager>();

        if (raceManager)
        {
            if(playerIndex > RaceManager.Instance.PlayerNumber)
            {
                DestroyImmediate(gameObject);
                return;
            }
            splitScreen = RaceManager.Instance.Players.Count == 2;
            playerName = RaceManager.Instance.PlayersName[playerIndex - 1];
            carData = RaceManager.Instance.Players[playerName];
        }
        else if (!carData)
        {
            DestroyImmediate(gameObject);
            return;
        }
    }
    private void Start()
    {
        // Delete child of empty object
        foreach (Transform child in transform)
        {
            if (Application.isEditor)
            {
                DestroyImmediate(child.gameObject);
            }
            else
            {
                Destroy(child.gameObject);
            }
        }
        GameObject visuals = Instantiate(carData.carPrefabColliders, transform.position, rotationOnStart);
        visuals.transform.SetParent(transform);
        visuals.AddComponent<AntiRollBar>();
        visuals.AddComponent<CarController>();
        visuals.GetComponent<CarController>().playerIndex = playerIndex;
        ApplyStats(visuals);

        Camera camera = CreateCarCameraStable(visuals);
        Canvas HUD = AddHUD(visuals, camera);
        ApplyHUDRules(visuals, camera, HUD);
        SetCameraDisplay(visuals, camera);
    }

    private void ApplyHUDRules(GameObject car, Camera camera, Canvas HUD)
    {
        HUD.name = $"HUD {car.GetComponent<CarController>().playerIndex}";
        HUD.GetComponent<HUDHandler>().carControl = this;

        GameObject cameraUI = new GameObject("HUDCamera", typeof(Camera));
        cameraUI.transform.SetParent(camera.transform);
        cameraUI.transform.localPosition = Vector3.zero;
        cameraUI.transform.rotation = Quaternion.identity;
        cameraUI.layer = 5;
        cameraUI.GetComponent<Camera>().clearFlags = CameraClearFlags.Depth;
        cameraUI.GetComponent<Camera>().cullingMask = 1 << 5;

        HUD.renderMode = RenderMode.ScreenSpaceCamera;
        HUD.worldCamera = GetComponentsInChildren<Camera>()[1];
    }

    private Canvas AddHUD(GameObject car, Camera camera)
    {
        Canvas HUD;
        if(splitScreen)
        {
            if(car.GetComponent<CarController>().playerIndex == 1)
            {
                HUD = Instantiate(Resources.Load<Canvas>("Canvas/HUDSplit1"));
            }
            else
            {
                HUD = Instantiate(Resources.Load<Canvas>("Canvas/HUDSplit2"));
            }
        }
        else
        {
            HUD = Instantiate(Resources.Load<Canvas>("Canvas/HUDFull"));
        }
        return HUD;
    }

    private Camera CreateCarCameraStable(GameObject car)
    {
        GameObject cubeFocus = GameObject.CreatePrimitive(PrimitiveType.Cube);
        Destroy(cubeFocus.GetComponent<BoxCollider>());
        cubeFocus.GetComponent<MeshRenderer>().enabled = false;
        cubeFocus.transform.SetParent(car.transform);
        cubeFocus.transform.localPosition = new Vector3(0, 2, 0);
        cubeFocus.transform.rotation = rotationOnStart;
        GameObject cameraGameObject = new GameObject("CarCamera", typeof(Camera));
        cameraGameObject.AddComponent<CameraStable>();
        cameraGameObject.transform.SetParent(cubeFocus.transform);
        cameraGameObject.transform.localPosition = new Vector3(0, 0, -6);
        cameraGameObject.transform.rotation = Quaternion.identity;
        cameraGameObject.GetComponent<Camera>().cullingMask = ~(1 << 5); ;

        return cameraGameObject.GetComponent<Camera>();
    }

    private void SetCameraDisplay(GameObject car, Camera camera)
    {
        if(splitScreen)
        {
            Rect temp = new Rect(0, 0, 1, 1);
            switch (car.GetComponent<CarController>().playerIndex)
            {
                case 1:
                    temp = new Rect(0, 0.5f, 1, 0.5f);
                    break;
                case 2:
                    temp = new Rect(0, 0, 1, 0.5f);
                    break;
            }
            camera.rect = temp;
        }

    }

    private void ApplyStats(GameObject gameObject)
    {
        gameObject.GetComponent<CarController>().motorForce = carData.motorForce;
        gameObject.GetComponent<CarController>().brakeForce = carData.brakeForce;
        gameObject.GetComponent<CarController>().maxSteeringAngle = carData.maxSteeringAngle;
    }
}
