using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDHandler : MonoBehaviour
{
    private int countdownTime = 3;
    private Text timeLap;
    private Text position;
    private Text lapNumber;
    private Text countdown;
    public CarControl carControl;

    private void Start()
    {
        foreach (Text textUI in GetComponentsInChildren<Text>())
        {
            switch (textUI.name)
            {
                case "TimeLap":
                    timeLap = textUI;
                    break;
                case "Position":
                    position = textUI;
                    break;
                case "Lap Number Value":
                    lapNumber = textUI;
                    break;
                case "Player Name":
                    textUI.text = carControl.playerName;
                    break;
                case "Countdown":
                    countdown = textUI;
                    break;
                default:
                    break;
            }
        }
        StartCoroutine(CountdownStart());

    }
    private void FixedUpdate()
    {
        LapController lapController = FindObjectOfType<LapController>();
        timeLap.text = lapController.StopwatchCars[carControl.gameObject].Elapsed.ToString(@"mm\:ss\:fff");
        switch (lapController.PositionCars[carControl.gameObject])
        {
            case 1:
                position.text = "1st";
                position.color = Color.yellow;
                break;
            case 2:
                position.text = "2nd";
                position.color = Color.white;
                break;
            default:
                break;
        }
        lapNumber.text = $"{lapController.CurrentLapNumber[carControl.gameObject]}/{lapController.maxLapNumber}";
    }

    IEnumerator CountdownStart()
    {
        carControl.GetComponentInChildren<CarController>().canMove = false;
        carControl.GetComponentInChildren<CarController>().currentBrakeForce = 1000000f;
        carControl.GetComponentInChildren<CarController>().ApplyBreakingForce();
        while (countdownTime > 0)
        {
            countdown.text = countdownTime.ToString();
            yield return new WaitForSeconds(1f);
            countdownTime--;
        }
        countdown.text = "GO!";
        carControl.GetComponentInChildren<CarController>().currentBrakeForce = 1000000f;
        carControl.GetComponentInChildren<CarController>().ApplyBreakingForce();
        FindObjectOfType<LapController>().StopwatchCars[carControl.gameObject].Start();
        carControl.GetComponentInChildren<CarController>().canMove = true;
        yield return new WaitForSeconds(1f);

        countdown.gameObject.SetActive(false);
    }
}
